package com.satya.money.analysing;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.satya.money.entity.CandleStick;;

public class AnalysingEqualAmoutTrendReversalConfirmationStrategyTest {
	AnalysingEqualAmoutTrendReversalConfirmationStrategy sut;
	@Before
	public void setUp(){
		sut = new AnalysingEqualAmoutTrendReversalConfirmationStrategy();
	}
	@Test
	public void testGetNegativeCount() {
		String dataFilePath = ".\\test\\test.csv";
		int expected = 2;
		sut.getDataUtil().setDataFilePath(dataFilePath);
		List<CandleStick> csList = sut.getDataUtil().loadDataFromCSVFile();
//		csList.forEach(System.out::println);
		int actual = sut.getNegativeCount(csList);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetPositiveCount() {
		String dataFilePath = ".\\test\\test1.csv";
		int expected = 3;
		sut.getDataUtil().setDataFilePath(dataFilePath);
		List<CandleStick> csList = sut.getDataUtil().loadDataFromCSVFile();
//		csList.forEach(System.out::println);
		int actual = sut.getPositiveCount(csList);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testIsBuyTradeTrue() {
		boolean expected = true;
		CandleStick prePreviousCS = new CandleStick();
		prePreviousCS.setClosingPrice(1.3);
		prePreviousCS.setOpeningPrice(1.2);
		CandleStick previousCS = new CandleStick();
		previousCS.setClosingPrice(1.3);
		previousCS.setOpeningPrice(1.1);
		boolean actual = sut.isBuyTrade(prePreviousCS, previousCS);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void testIsBuyTradeFalse() {
		boolean expected = false;
		CandleStick prePreviousCS = new CandleStick();
		prePreviousCS.setClosingPrice(1.2);
		prePreviousCS.setOpeningPrice(1.3);
		CandleStick previousCS = new CandleStick();
		previousCS.setClosingPrice(1.1);
		previousCS.setOpeningPrice(1.3);
		boolean actual = sut.isBuyTrade(prePreviousCS, previousCS);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testIsTradeExecutedTrue() {
		boolean expected = true;
		CandleStick prePreviousCS = new CandleStick();
		prePreviousCS.setClosingPrice(1.2);
		prePreviousCS.setOpeningPrice(1.3);
		CandleStick previousCS = new CandleStick();
		previousCS.setClosingPrice(1.1);
		previousCS.setOpeningPrice(1.3);
		boolean actual = sut.isTradeExecuted(prePreviousCS, previousCS);
		Assert.assertEquals(expected, actual);
	}
	@Test
	public void testIsTradeExecutedFalse() {
		boolean expected = false;
		CandleStick prePreviousCS = new CandleStick();
		prePreviousCS.setClosingPrice(1.2);
		prePreviousCS.setOpeningPrice(1.3);
		CandleStick previousCS = new CandleStick();
		previousCS.setClosingPrice(1.3);
		previousCS.setOpeningPrice(1.1);
		boolean actual = sut.isTradeExecuted(prePreviousCS, previousCS);
		Assert.assertEquals(expected, actual);
	}

}
