package com.satya.money.analysing;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.satya.money.entity.CandleStick;

public class AnalysingEqualAmountStrategyTest {
	AnalysingEqualAmountStrategy sut;
	@Before
	public void setUp(){
		sut = new AnalysingEqualAmountStrategy();
	}
	@Test
	public void testGetNegativeCount() {
		String dataFilePath = ".\\test\\test3.csv";
		int expected = 6;
		sut.getDataUtil().setDataFilePath(dataFilePath);
		List<CandleStick> csList = sut.getDataUtil().loadDataFromCSVFile();
//		csList.forEach(System.out::println);
		int actual = sut.getNegativeCount(csList);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetPositiveCount() {
		String dataFilePath = ".\\test\\test2.csv";
		int expected = 7;
		sut.getDataUtil().setDataFilePath(dataFilePath);
		List<CandleStick> csList = sut.getDataUtil().loadDataFromCSVFile();
//		csList.forEach(System.out::println);
		int actual = sut.getPositiveCount(csList);
		Assert.assertEquals(expected, actual);
	}

}
