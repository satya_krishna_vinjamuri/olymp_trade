package com.satya.money.analysing;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.satya.money.common.ApplicationConstants;
import com.satya.money.entity.CandleStick;
import com.satya.money.loggin.Logger;
import com.satya.money.util.DataUtil;

public abstract class Strategy {
	private DataUtil dataUtil = new DataUtil(ApplicationConstants.DATA_FILE_PATH);

	public DataUtil getDataUtil() {
		return dataUtil;
	}

	public void setDataUtil(DataUtil dataUtil) {
		this.dataUtil = dataUtil;
	}
	
	public boolean isIndicisive(CandleStick cs){
		return (!cs.isBearish() && !cs.isBullish());
	}
	
	public abstract int getNegativeCount(List<CandleStick> csList);
	
	public abstract int getPositiveCount(List<CandleStick> csList);
	
	public float getPositivePercent(List<CandleStick> csList){
		return ((float)getPositiveCount(csList))/csList.size() * 100;
	}
	public float getNegativePercent(List<CandleStick> csList){
		return ((float)getNegativeCount(csList))/csList.size() * 100;
	}
	
	public void printPercentagesForAPeriod(String from, String to){
		System.out.println("Positive Percent:" +
				getPositivePercent(dataUtil.loadDataBetweenTwoTimeStamps(from, to)));
		System.out.println("Negative Percent:" +
				getNegativePercent(dataUtil.loadDataBetweenTwoTimeStamps(from, to)));

	}
	
	public float getMaxPositivePercentageForIntervals(String from, String to, int min){
		float maxPercentage = 0.0f;
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try {
			Date fromTime = sdf.parse(from);
			Date toTime = sdf.parse(to);
			float curPercentage;
			Date startClone  = ((Date) fromTime.clone());
			startClone.setTime(startClone.getTime() + min * 60000);
			for(Date i = startClone; i.before(toTime);i.setTime(i.getTime() + min * 60000)){
				curPercentage = getPositivePercent(
						dataUtil.loadDataBetweenTwoTimeStamps(fromTime, i));
				Logger.write("from: " + fromTime + " to: " + i,"INFO");
				Logger.write(curPercentage + "", "INFO");
				fromTime.setTime(fromTime.getTime() + min * 60000);
				if(maxPercentage < curPercentage){
					maxPercentage = curPercentage;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return maxPercentage;
	}
	public float getMaxNegativePercentageForIntervals(String from, String to, int min){
		float maxPercentage = 0.0f;
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try {
			Date fromTime = sdf.parse(from);
			Date toTime = sdf.parse(to);
			float curPercentage;
			Date startClone  = ((Date) fromTime.clone());
			startClone.setTime(startClone.getTime() + min * 60000);
			for(Date i = startClone; i.before(toTime);i.setTime(i.getTime() + min * 60000)){
				curPercentage = getNegativePercent(
						dataUtil.loadDataBetweenTwoTimeStamps(fromTime, i));
				Logger.write("from: " + fromTime + "to: " + i,"INFO");
				Logger.write(curPercentage + "","INFO");
				fromTime.setTime(fromTime.getTime() + min * 60000);
				if(maxPercentage < curPercentage){
					maxPercentage = curPercentage;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return maxPercentage;
	}
	
	public void printPositiveAndNegativePercentageForIntervals(String from, String to, int min){
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try {
			Date fromTime = sdf.parse(from);
			Date toTime = sdf.parse(to);
			float curPositivePercentage;
			float curNegativePercentage;
			Date startClone  = ((Date) fromTime.clone());
			startClone.setTime(startClone.getTime() + min * 60000);
			for(Date i = startClone; i.before(toTime);i.setTime(i.getTime() + min * 60000)){
				curNegativePercentage = getNegativePercent(
						dataUtil.loadDataBetweenTwoTimeStamps(fromTime, i));
				curPositivePercentage = getPositivePercent(
						dataUtil.loadDataBetweenTwoTimeStamps(fromTime, i));
				Logger.write("from: " + fromTime + "to: " + i,"INFO");
				Logger.write("Positive: " + curPositivePercentage,"INFO");
				Logger.write("Negative: " + curNegativePercentage,"INFO");
				fromTime.setTime(fromTime.getTime() + min * 60000);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
