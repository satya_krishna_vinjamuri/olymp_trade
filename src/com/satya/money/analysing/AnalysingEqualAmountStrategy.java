package com.satya.money.analysing;

import java.util.List;

import com.satya.money.entity.CandleStick;

public class AnalysingEqualAmountStrategy extends Strategy{
	@Override
	public int getNegativeCount(List<CandleStick> csList){
		int count = 0;
		CandleStick previousCS;
		CandleStick currentCS = null;
		for(CandleStick cs: csList){
			if(isIndicisive(cs)) continue;
			previousCS = currentCS;
			currentCS = cs;
			if(previousCS != null && currentCS != null){
				if(previousCS.isBearish() != currentCS.isBearish()){
					count++;
				}
			}
		
		}
		return count;
	}
	@Override
	public int getPositiveCount(List<CandleStick> csList){
		int count = 0;
		CandleStick previousCS;
		CandleStick currentCS = null;
		for(CandleStick cs: csList){
			if(isIndicisive(cs)) continue;
			previousCS = currentCS;
			currentCS = cs;
			if(previousCS != null && currentCS != null){
				if(previousCS.isBearish() == currentCS.isBearish()){
					count++;
				}
			}
		
		}
		return count;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AnalysingEqualAmountStrategy aems = new AnalysingEqualAmountStrategy();
		String from = "2020.05.01 00:00";
		String to = "2020.05.31 23:59";
//		aems.printPercentagesForAPeriod(from, to);
//		System.out.println("MaxPercentage: " + 
//		aems.getMaxSimilarPercentageForIntervals(from, to, 60));
//		aems.printPositiveAndNegativePercentageForIntervals(from, to, 30);
		aems.printPercentagesForAPeriod(from, to);


	}

}
