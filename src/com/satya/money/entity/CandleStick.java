package com.satya.money.entity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 
 * @author SATYA KRISHNA VINJAM
 * this class represents a Candle Stick
 */
public class CandleStick {
	private Date timeStamp;
	private double openingPrice;
	private double closingPrice;
	private double highPrice;
	private double lowPrice;
	private boolean isBullish;
	private boolean isBearish;
	
	public CandleStick(){
		
	}

	public CandleStick(Date timeStamp, double openingPrice, double highPrice, double lowPrice, double closingPrice) {
		super();
		this.timeStamp = timeStamp;
		this.openingPrice = openingPrice;
		this.closingPrice = closingPrice;
		this.highPrice = highPrice;
		this.lowPrice = lowPrice;
		updateCandleType();
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public double getOpeningPrice() {
		return openingPrice;
	}

	public void setOpeningPrice(double openingPrice) {
		this.openingPrice = openingPrice;
		updateCandleType();
	}

	public double getClosingPrice() {
		return closingPrice;
	}

	public void setClosingPrice(double closingPrice) {
		this.closingPrice = closingPrice;
		updateCandleType();
	}

	public double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(double highPrice) {
		this.highPrice = highPrice;
	}

	public double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public boolean isBullish() {
		return isBullish;
	}
	
	public boolean isBearish() {
		return isBearish;
	}
	
	private void updateCandleType(){
		if(openingPrice == 0.0 || closingPrice == 0.0){
			return;
		}
		if((openingPrice - closingPrice) > 0){
			isBearish = true;
			isBullish = false;
		}else if((openingPrice - closingPrice) < 0){
			isBearish = false;
			isBullish = true;
		}else{
			isBearish = false;
			isBullish = false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CandleStick other = (CandleStick) obj;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CandleStick [timeStamp=" + timeStamp + ", openingPrice=" + openingPrice + 
				", highPrice=" + highPrice + ", lowPrice=" + lowPrice +
				", closingPrice=" + closingPrice + ", isBullish=" + isBullish + ", isBearish=" + isBearish + "]";
	}

	public static void main(String[] args) throws ParseException{
		String pattern = "yyyy-MM-dd HH:mm";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date = sdf.parse("2020-06-01 03:55");
		CandleStick cs = new  CandleStick(date ,1.114620000,1.114720000,1.114480000,1.114590000);
		System.out.println(cs);
	}
	
}
