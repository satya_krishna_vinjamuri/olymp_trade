package com.satya.money.loggin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import com.satya.money.common.ApplicationConstants;

public class Logger {
	public static void write(String line, String type) throws IOException{
		File file = new File(ApplicationConstants.LOGGER_OUTPUT_FILE_PATH);
		if(!file.exists()){
			throw new FileNotFoundException("couldn't find file:" + file);
		}
		BufferedWriter out = new BufferedWriter(
				new FileWriter(
						new File(ApplicationConstants.LOGGER_OUTPUT_FILE_PATH),true));
		out.write("\n[" + type +"]: " +line);
		out.close();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			write("first hello","INFO");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
