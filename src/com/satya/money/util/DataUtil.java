package com.satya.money.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.satya.money.common.ApplicationConstants;
import com.satya.money.entity.CandleStick;
import com.satya.money.loggin.Logger;

public class DataUtil {
	private String dataFilePath;
	
	private static int index = 2; // starts parsing line from this index.
	
	public static String fetchDate(String[] records){
		return records[0] + " " + records[1];
	}
	
	public DataUtil(String dataFilePath) {
		super();
		this.dataFilePath = dataFilePath;
	}

	public String getDataFilePath() {
		return dataFilePath;
	}

	public void setDataFilePath(String dataFilePath) {
		this.dataFilePath = dataFilePath;
	}

	/**
	 * 
	 * @param line -> expects a line that is read from data file.
	 * ex: "2020-06-01 03:55,1.114620000,1.114720000,1.114480000,1.114590000,.0000"
	 * and it is read in the format -> "timeStamp, openingPrice, maxPrice, minPrice, closingPrice, Volume"
	 * and remaining are left out.
	 */
	public static CandleStick getCandleStickOutOfLineInDataFile(String line){
		CandleStick candleStick = new CandleStick();
		String[] data = line.split(",");
		if(data.length != ApplicationConstants.RECORD_LENGTH){
			throw new IllegalArgumentException("passed line doesn't contain data in below format:\n" +
					"timeStamp, openingPrice, maxPrice, minPrice, closingPrice" +
					"2020-06-01 03:55,1.114620000,1.114720000,1.114480000,1.114590000,.0000");
		}
		SimpleDateFormat formatter = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try {
			candleStick.setTimeStamp(formatter.parse(fetchDate(data)));
			candleStick.setOpeningPrice(Double.parseDouble(data[index]));
			candleStick.setHighPrice(Double.parseDouble(data[index + 1]));
			candleStick.setLowPrice(Double.parseDouble(data[index + 2]));
			candleStick.setClosingPrice(Double.parseDouble(data[index + 3]));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return candleStick;
	}
	
	public List<CandleStick> loadDataFromCSVFile(){
		List<CandleStick> listCandleStick = new ArrayList<>();
		String line = "";
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(dataFilePath)))){
			while((line = reader.readLine()) != null){
				try{
					listCandleStick.add(getCandleStickOutOfLineInDataFile(line));
				}catch(IllegalArgumentException exception){
					// TODO add loggers
					System.out.println("Line in data file that caused exception:\n" + line);
					exception.printStackTrace();
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCandleStick;
	}
	
	public List<CandleStick> loadDataBetweenTwoTimeStamps(String fromTimeStamp, String toTimeStamp){
		List<CandleStick> listCandleStick = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(dataFilePath)))){
			Date fromTime = sdf.parse(fromTimeStamp);
			Date toTime = sdf.parse(toTimeStamp);
			loadDataFromFile(listCandleStick, sdf, reader, fromTime, toTime);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCandleStick;
	}
	
	public List<CandleStick> loadDataBetweenTwoTimeStamps(Date fromTime, Date toTime){
		List<CandleStick> listCandleStick = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(dataFilePath)))){
			loadDataFromFile(listCandleStick, sdf, reader, fromTime, toTime);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCandleStick;
	}

	private void loadDataFromFile(List<CandleStick> listCandleStick, SimpleDateFormat sdf, BufferedReader reader,
			Date fromTime, Date toTime) throws Exception {
		String line = "";
		String[] records;
		Date currentTimeStamp;
		boolean isFromTimeFound = false;
		while((line = reader.readLine()) != null){
			records = line.split(",");
			try{
				if(records.length == ApplicationConstants.RECORD_LENGTH){
					currentTimeStamp = sdf.parse(fetchDate(records));
					if(currentTimeStamp.after(fromTime)){
						break;
					}
					if(fromTime.compareTo(currentTimeStamp) == 0){
						isFromTimeFound = true;
						listCandleStick.add(getCandleStickOutOfLineInDataFile(line));
						do{
							line = reader.readLine();
							if(line == null){
								Logger.write("tODate Not Found: " + toTime,"ERROR");
								return;
							}
							records = line.split(",");
							if(records.length == ApplicationConstants.RECORD_LENGTH){
								currentTimeStamp = sdf.parse(fetchDate(records));
								if(currentTimeStamp.after(toTime)){
									Logger.write("tODate Not Found: " + toTime,"ERROR");
									return;
								}
								listCandleStick.add(getCandleStickOutOfLineInDataFile(line));
							}
						}while(toTime.compareTo(currentTimeStamp) != 0);
					}
				}
				
			}catch(IllegalArgumentException exception){
				// TODO add loggers
				Logger.write("Line in data file that caused exception:\n" + line,"ERROR");
				exception.printStackTrace();
			}
		}
		if(!isFromTimeFound){
			Logger.write("fromDate Not Found: " + fromTime,"ERROR");
			return;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataUtil dataUtil = new DataUtil("C:\\Users\\SATYA KRISHNA VINJAM\\Desktop\\test.data");
		String line = "2020-06-01 03:55,1.114620000,1.114720000,1.114480000,1.114590000,.0000";
//		System.out.println(DataUtil.getCandleStickOutOfLineInDataFile(line));
//		dataUtil.loadDataFromCSVFile().forEach(System.out::println);
		dataUtil.loadDataBetweenTwoTimeStamps("2020-06-01 05:26", "2020-06-02 05:37").
		forEach(System.out::println);

	}

}
