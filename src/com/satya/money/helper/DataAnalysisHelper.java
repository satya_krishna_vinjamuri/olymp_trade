package com.satya.money.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.satya.money.common.ApplicationConstants;
import com.satya.money.entity.CandleStick;
import com.satya.money.util.DataUtil;

public class DataAnalysisHelper {
	private DataUtil dataUtil = new DataUtil(ApplicationConstants.DATA_FILE_PATH);

	public DataUtil getDataUtil() {
		return dataUtil;
	}

	public void setDataUtil(DataUtil dataUtil) {
		this.dataUtil = dataUtil;
	}
	private boolean isIndicisive(CandleStick cs){
		return (!cs.isBearish() && !cs.isBullish());
	}
	public int getNegativeCount(List<CandleStick> csList){
		int count = 0;
		CandleStick previousCS;
		CandleStick currentCS = null;
		for(CandleStick cs: csList){
			if(isIndicisive(cs)) continue;
			previousCS = currentCS;
			currentCS = cs;
			if(previousCS != null && currentCS != null){
				if(previousCS.isBearish() != currentCS.isBearish()){
					count++;
				}
			}
		
		}
		return count;
	}
	public int getPositiveCount(List<CandleStick> csList){
		int count = 0;
		CandleStick previousCS;
		CandleStick currentCS = null;
		for(CandleStick cs: csList){
			if(isIndicisive(cs)) continue;
			previousCS = currentCS;
			currentCS = cs;
			if(previousCS != null && currentCS != null){
				if(previousCS.isBearish() == currentCS.isBearish()){
					count++;
				}
			}
		
		}
		return count;
	}
	
	public float getPositivePercent(List<CandleStick> csList){
		return ((float)getPositiveCount(csList))/csList.size() * 100;
	}
	public float getNegativePercent(List<CandleStick> csList){
		return ((float)getNegativeCount(csList))/csList.size() * 100;
	}
	
	public void getPercentagesForAPeriod(String from, String to){
		System.out.println("Flucuation Percent:" +
				getNegativePercent(dataUtil.loadDataBetweenTwoTimeStamps(from, to)));
		System.out.println("Positive Percent:" +
				getPositivePercent(dataUtil.loadDataBetweenTwoTimeStamps(from, to)));
	}
	
	public float getMaxSimilarPercentageForIntervals(String from, String to, int min){
		float maxPercentage = 0.0f;
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConstants.TIME_STAMP_FORMAT);
		try {
			Date fromTime = sdf.parse(from);
			Date toTime = sdf.parse(to);
			float curPercentage;
			Date startClone  = ((Date) fromTime.clone());
			startClone.setTime(startClone.getTime() + min * 60000);
			for(Date i = startClone; i.before(toTime);i.setTime(i.getTime() + min * 60000)){
				curPercentage = getPositivePercent(
						dataUtil.loadDataBetweenTwoTimeStamps(fromTime, i));
				System.out.println("from: " + fromTime + "to: " + i);
				System.out.println(curPercentage);
				fromTime.setTime(fromTime.getTime() + min * 60000);
				if(maxPercentage < curPercentage){
					maxPercentage = curPercentage;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return maxPercentage;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
